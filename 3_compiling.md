# What is compiling:
Is a process that transform Human Readable Codes into Machine-lang
It is composeb by 4 main part:
- Preprocessing: pocessing of # charachter, remove comments, all non-useful stuff,...
- Compilation: Produce semi-readable code (ASSEMBLY)
- Assembly: Assembly code is processed to create an OBJECT CODE, binary code readable from processor
- Linking: Assembler (one of your choice) ink all external/out of order libraries to original code (Final stage)

## Command to compile in linux: gcc
GNU project C and C++ compiler
```bash
gcc [options] file..
```

[options] : you can see one of the prevous stages with specific options:

- Preprocessing: -E (as manual say..) Stop after the preprocessing stage; do not run the compiler proper. Print in stdout
- Compilation: -S Stop after the stage of compilation proper; do not assemble. It create .s file 
- Assembly: -c Compile or assemble the source files, but do not link. It create .o file

### Exercise 1
See the all the compiling stages whits this hello-world:
```C
#include <stdio.h> // What does it mean to include a lib?
int main(){
    int p = 0; // Just to see wath preprocessing do (or not-do)
	if (p) \
        printf("Hello World");
	return 0;
}
```
### Exercise 2
Do the same with an OpenMP call (stop at Compilation)

```C
#include"stdio.h"
#include"omp.h"

int main()
{
	#pragma omp parallel // providing additional information to the compiler
	{
		printf("\nHello World!");
	}
	printf("\nProgram Exit!\n");
}
```
now try to redo the operation with:
```bash
gcc -fopenmp yourfile.c 
```

Have you noticed anything?
