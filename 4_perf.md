# Perf
Official Linux profiler built on top of kernel infrastructure (ftrace)

## Performance Monitoring Unit (PMU)
Processor usually support a large number of events (cycles, instructions retired, etc.) through Performance Monitoring Counters (PMC).
They can be:
- On-core: this PMU instrument the microarchitecture of the CPU reporting events at core level (instructions retired, cycles,
front-end/back-end metrics, L1/L2 cache information, etc.)
- Off-core: this PMU handle performance counters outside of cores (LLC, on-chip interconnect, DRAM, Power, etc.). Beware,
usually off-core performance counters need special privileges to be read (security issues).

Moreover they can be:
- Fixed: can be only enabled or disable and profile a specific event (e.g. cycles, instructions retired, etc.)
- Configurable: usually can monitor a large number of events (cache hit/miss, branch taken, FLOPs, etc.)

They are usually accessible only from the kernelspace, often the CPU support the reading of the PMU through specific userspace assembly instructions with low overhead (see rdpmc())
### Common metrics

- Cycles: count the number of cycles
- instructions retired: count the number of macro instructions executed
- µops retired: count the number of micro instructions executed
- branches: count the number of branch taken
- vector instructions retired: count the number of vector macro instructions
- instructions per cycle (IPC): count the number of macro instructions executed each cycle -> this metric show the macro
instruction throughput of the microarchitecture
- µops per cycle: count the number of micro instructions executed each cycle -> it show the micro instruction throughput of
the microarchitecture
- branch-misses: branches that has not been correctly predicted from the branch prediction unit (BPU) -> at every branch miss
the pipeline is wiped out
- cache references (at multiple cache levels): increment this PMC every time a cache has been queried to request a cache line
- cache miss/hit (at multiple cache levels): count the miss/hit of the cache references -> it show the locality of the code
- vectorization ratio: show the ratio between all the macro instructions retired with respect to the vector macro instructions ->
high performance codes are highly vectorized
- FLOP/s: count the arithmetic operations executed of the macro instructions (no bitwise, move, load/store operations)
- power/energy: count the joule used by package/core/dram of the CPU -> it used to calculate the energy efficiency (Green500)
- memory throughput: count the number of cache line read/write from/to the main memory
## Perf tool

--- 
This option is useful for this tool:

-g  Produce debugging information in the operating system's native format (stabs, COFF, XCOFF, or DWARF).  GDB can work with this debugging information.
## List
## Stat
## Record
## Report



