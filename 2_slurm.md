# Slurm
https://slurm.schedmd.com/overview.html

What is slurm?

_Slurm is a open-source cluster management and job scheduling system for Linux Cluster_

1. Keep track of available resoruce on cluster
2. Collect users' requests for job
3. Assign proprieties to jobs
4. Run jobs on assigned nodes

![](./img/slurm.png)

## Partitions
Compute nodes are grouped into logical set called **partitions** depending on their hardware or specific functions.
In mcimone we have 2 partitions:
- Sifive nodes (mcimone-nodes) 8x(4-core, 64bit, RISCV, 15GB RAM)
- Milkv nodes (mcimone-milkvs) 3x(64-core, 64bit, RISCV, 128GB RAM)

with the command `sinfo` you get all slurm node's (and also partitions) informations.

## How to allocate a job

In order to schedule/allocate some resources in a cluster you need to interact with slurm, and not with the machines itself.
There's 3 way to allocate a resources/jobs:
- Run a SBATCH script (like bash, but with some slurm's commands): `sbatch`
- Directly run command inside nodes: `srun`
- Allocating nodes for certain amount of time:`salloc` + `srun`


### SBATCH
Slurm job script work like a normal script but with slurm directives:

```bash
#!/usr/bin/bash
#SLURM directives
env variables
exec lines
```
an example could be
```bash
#/usr/bin/bash
#SBATCH --job-name=test
#SBATCH --nodes=1
#SBATCH -exlusive
#SBATCH --time=00:20:00
#SBATCH --output=output.txt
#SBATCH --error=output_error.txt
srun echo "i'm working on Monte Cimone!"
```
check documentation: https://slurm.schedmd.com/sbatch.html

### SRUN


```bash
srun -n <n_task_to_allocate> [-N <n_nodes_to_run> ] [-p <partitions>] [-w <specific_node>] [-t <hours:minutes:seconds>] [--pty] command
```
some example:
```bash
# To understand what does it means to run on 4 different task (not using 4 core to run single command!!)
srun -n 4 hostname
srun -N 4 hostname
srun -n 32 hostname

# Try to allocate on another partitions
srun -n 64 -p mcimone-milkvs -w mcimone-milkv-1 hostname

# Will be killed by time-limit
srun -n 1 -t 00:01 sleep 1000
```

check other params on documentation: https://slurm.schedmd.com/srun.html


### SALLOC

```bash
salloc -n <> -t <hours:minutes:seconds> [-p <>] [-w <>] [--exclusive]
```
then after allocation 

```bash
srun [-n <> (default is the allocated ones)] command 
# Or directly access with ssh (if ssh connection use --exlusive with salloc)
ssh username@host-allocated
```

some example:
```bash
# Allor 10 tasks for 10 minutes
salloc -n 10 -t 10:00

# Let slurm show what node allocate to us
srun hostname

```

check documentation: https://slurm.schedmd.com/salloc.html 

## Check slurm information

```bash
sinfo # to obtain information about nodes and partitions (see if is used, if is down, drained etc.)
squeue # To check what is queue of slurm (see if u have allocated jobs, when will run etc.)
scancel <JOBID> # cancel specific job (to obtain jobid see squeue)
```