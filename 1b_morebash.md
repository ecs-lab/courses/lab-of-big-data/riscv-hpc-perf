# Bash commands
bible of bash: https://www.gnu.org/software/bash/manual/bash.html#Shell-Commands
Special carachetr
- \$!: bash script parameter is used to reference the process ID of the most recently executed command in background.
- \$$: is used to reference the process ID of bash shell itself
- \$#: is quite a special bash parameter and it expands to a number of positional parameters in decimal.
- $0: bash parameter is used to reference the name of the shell or shell script. so you can use this if you want to print the name of shell script.
- $-:(dollar hyphen) bash parameter is used to get current option flags specified during the invocation, by the set built-in command or set by the bash shell itself. Though this bash parameter is rarely used.
- $?: is one of the most used bash parameters and used to get the exit status of the most recently executed command in the foreground. By using this you can check whether your bash script is completed successfully or not.
- \$_: (dollar underscore) is another special bash parameter and used to reference the absolute file name of the shell or bash script which is being executed as specified in the argument list. This bash parameter is also used to hold the name of mail file while checking emails.
- \$@: (dollar at the rate) bash parameter is used to expand into positional parameters starting from one. When expansion occurs inside double-quotes, every parameter expands into separate words.
- \$*: (dollar star) this is similar to $@ special bash parameter  only difference is when expansion occurs with double quotes, it expands to a single word with the value of each bash parameter separated by the first character of the IFS special environment variable.


- $()
- ; in bash permit to have more than one command in line
### Enviroment
` When a program is invoked it is given an array of strings called the environment. This is a list of name-value pairs, of the form name=value.
The shell provides several ways to manipulate the environment. On invocation, the shell scans its own environment and creates a parameter for each name found, automatically marking it for export to child processes. Executed commands inherit the environment. The export and declare  -x commands allow parameters and functions to be added to and deleted from the environment. If the value of a parameter in the environment is modified, the new value becomes part of the environment, replacing the old. The environment inherited by any executed command consists of the shell's initial environment, whose values may be modified in the shell, less any pairs removed by the unset command, plus any additions via the export and declare -x commands.`
## More Bash
```bash
myvariable=whateveriwant_both_string_or_nubers
export myvariable
USAGE="$0 folder" #It's a variable that change when $0 (name of the running command) change 

# IF 
#if [ COND ]; then
    #...
#[elif [ COND ]; then]
#[else]
#fi
# condition could be 
# [!:not]
# -lt|gt: less|great then (two numbers)
# -le: less or equal (two numbers)
# -eq: equal (two numbers)
# -e: exists
# -d check if is directory
# -f check if regular file
# -r|w|x readable|writable|executable file
# -nt newer than (two files)
# -ot older than (two files)
# == | != check two strings are equals|different


# FOR
#for VAR in SOMETHING_ITERABLE
#do
    #...
#done

# CASE
#case VAR in
#  PATTERN_1)
#    STATEMENTS
#    ;;
#esac

# let's do some example: i want to verify that input parameters is bigger than 1
if [ $# -lt 1 ];then
    echo $USAGE
    return 1 # Return error!
fi


# For example i want to inspect all file in a folder
cd $1
for var in *; do
    if [ -f $var ]; then
        echo $var is a file
    fi
    case $var in
        *.c*)
            echo $var is a c file
        ;;
    esac
done


```

# Redirect
useful link: https://www.gnu.org/software/bash/manual/bash.html#Redirections, https://en.wikipedia.org/wiki/Standard_streams

Most used:
- cmd1 > X : stdout to file
- cmd1 | cmd2 : stout of cmd1 to stdin of cmd2 
- &2>1 stderror redirected to stdout 

# Exercise
1. Do a script that explore all folder (handle more than one) given in input and print file in following order: 
    - executable files
    - not writable files
    - readable and writable

2. Do a script that recoursively explore all subfolder starting from current ones, then if find a file without extension, and if not executable, make it executable and print in stdout all file modified.

3. Make a script that recognize if input arguments has an absolute(if absolute path is given, check if exsists or is fake one) or relative path:
    - ex: absorrel.sh nonabs /absolute ./nonabs
